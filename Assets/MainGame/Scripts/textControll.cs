﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textControll : MonoBehaviour
{
    Text targettext;
    GameObject gcon;
    GameControll gc;
    // Start is called before the first frame update
    void Start()
    {
        gcon =GameObject.Find("GameControll");
        gc = gcon.GetComponent<GameControll>();
        targettext = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        if (gc.numCoin > 0)
        {
            string ontext = ("Coins : " + gc.numCoin + "/" + gc.allCoin);
            targettext.text = ontext;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
