﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JudgeGravityRotate : MonoBehaviour
{
    [SerializeField] private float time = 10f;

    private float deftime;
    private SphereControll sc;
    private GameObject sphere;
    // Start is called before the first frame update
    void Start()
    {
        deftime = time;

        sphere = GameObject.Find("Sphere");
        sc = sphere.GetComponent<SphereControll>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (sc.rotFlag == false)
        {
            time -= Time.deltaTime;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        sc.rotFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (time <= 0)
        {
            sc.rotFlag = true;
            time = deftime;
        }
    }
}
