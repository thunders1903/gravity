﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityControll : MonoBehaviour
{
    [SerializeField] private float gravityScale = 1.0f;
    [SerializeField] private bool antiGravity = false;
    public bool rotFlag = false;
    public bool offFlag = false;

    // Start is called before the first frame update
    void Start()
    {
        if (antiGravity == true)
        {
            gravityScale *= -1f;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (offFlag == false)
        {
            Debug.Log(collision);
            Rigidbody2D rig;
            rig = collision.GetComponent<Rigidbody2D>();

            Vector2 vec = transform.position - collision.transform.position;
            float dis = Vector2.Distance(transform.position, collision.transform.position);
            if (dis >= 0.02)
            {
                rig.AddForce(vec * (1 / Mathf.Pow(dis, 2)) * gravityScale);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
