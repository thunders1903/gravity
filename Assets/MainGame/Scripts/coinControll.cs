﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinControll : MonoBehaviour
{

    GameControll gc;
    GameObject gcon;
    [SerializeField]GameObject lastObject;
    private bool lFlag=false;
    [SerializeField] float rotatespeed;
    GameObject coin;
    // Start is called before the first frame update
    void Start()
    {
        gcon = GameObject.Find("GameControll");
        gc = gcon.GetComponent<GameControll>();
        coin = transform.Find("coin").gameObject;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {   
            GameObject.Destroy(this.gameObject);
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        transform.eulerAngles=transform.eulerAngles + new Vector3(0,0,rotatespeed);
        if(gc.lastFlag== true && lFlag==false)
        {
            GameObject.Destroy(coin.gameObject);
            Instantiate(lastObject,transform);
            
            lFlag = true;
        }
    }
}
