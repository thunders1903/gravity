﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll : MonoBehaviour
{
    private GameObject sphere;
    // Start is called before the first frame update
    void Start()
    {
        sphere = GameObject.Find("Sphere");
    }

    // Update is called once per frame
    void Update()
    {
        float x = sphere.transform.position.x;
        transform.position = new Vector3(x, 0, -10);
    }
}
