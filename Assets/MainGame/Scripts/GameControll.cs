﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControll : MonoBehaviour
{
    public int numCoin;
    public int allCoin;
    public bool lastFlag = false;
    public bool clearFlag = false;
    [SerializeField] private  GameObject clear;

    GameObject[] tagObjects;
    // Start is called before the first frame update
    void Start()
    {
        clear = GameObject.Find("Clear");
        clear.SetActive(false);
        Check("coin");
        allCoin = numCoin;

    }

    // Update is called once per frame
    void Update()
    {
        Check("coin");
        if (numCoin <= 1 )
        {
            lastFlag = true;
        }
        if (numCoin <= 0)
        {
            clearFlag = true;
        }
        if (clearFlag == true)
        {
            clear.SetActive(true);    
        }
    }
    void Check(string tagname)
    {
        tagObjects = GameObject.FindGameObjectsWithTag(tagname);
        numCoin = tagObjects.Length;
    }
}
