﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereControll : MonoBehaviour
{
    public bool controllFlag = true;
    private Rigidbody2D rig;
    [SerializeField]private float speed=1f;
    [SerializeField] private float downSpeed;
    [SerializeField] private float lastSpeed;
    [SerializeField] private float stopSpeed;
    [SerializeField] private float stopDownSpeed;
    private GravityControll gc;
    private bool tFlag=false;
    private bool stFlag=true;
    public bool rotFlag = false;
    // Start is called before the first frame update
    void Start()
    {
        rig=GetComponent<Rigidbody2D>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    { 
        gc = collision.GetComponent<GravityControll>();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (tFlag == false)
        {
            gc.offFlag = false;
            gc = null;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        gc = collision.GetComponent<GravityControll>();
        tFlag = true;

    }

    private void FixedUpdate()
    {

        
        tFlag = false;
    }
    // Update is called once per frame
    void Update()
    {
      
        if (rig.velocity == Vector2.zero)
        {
            stFlag = true;
        }
        else
        {
            stFlag = false;
        }
        if (controllFlag == true && (stFlag == true||rotFlag==true))
        {
            Vector2 vec = Vector2.zero;
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mouse=Input.mousePosition;
                Vector2 mpos= Camera.main.ScreenToWorldPoint(mouse);
                Debug.Log(mpos);
                vec = mpos - (Vector2)transform.position;
                rig.AddForce(vec * speed);
                if (gc != null)
                {
                    gc.offFlag = true;
                }
            }

            
        }
        if (tFlag == false)
        {
            rig.velocity = rig.velocity * downSpeed;

            if (Vector2.SqrMagnitude(rig.velocity) <= stopSpeed)
            {
                rig.velocity = rig.velocity * stopDownSpeed;
            }
            if (Vector2.SqrMagnitude(rig.velocity) <= lastSpeed)
            {
                rig.velocity = Vector2.zero;
            }
        }
    }
}
